import 'package:flutter/material.dart';
import 'package:flutter_app/pages/product_details.dart';

class Products extends StatefulWidget {
  @override
  _ProductsState createState() => _ProductsState();
}

class _ProductsState extends State<Products> {
  var product_list = [
    {
    "name":"Christmas Card",
    "picture":"images/handmadecard3.jpg",

    "price":20,
  },
    {
      "name":"Birthday Card",
      "picture":"images/card6.jpg",

      "price":10,
    },
    {
      "name":"Greeting Card",
      "picture":"images/Handmade-card2.jpg",
      "price":15,
    },
    {
      "name":"Thankyou Card",
      "picture":"images/card7.jpg",
      "price":16,
    },


  ];
  @override
  Widget build(BuildContext context) {
    return GridView.builder
      (itemCount: product_list.length,
        gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2),
        itemBuilder: (BuildContext context,int index){
        return Single_prod(
          prod_name: product_list[index]['name'],
          prod_picture: product_list[index]['picture'],
          prod_price: product_list[index]['price'],
        );
        }
        );
  }
}
class Single_prod extends StatelessWidget {
  final prod_name;
  final prod_picture;
  final prod_price;

   Single_prod({
     this.prod_name,
    this.prod_picture,
    this.prod_price
  }) ;
  @override
  Widget build(BuildContext context) {
    return Card(
      child:Hero(tag: prod_name,
          child: Material(
            child: InkWell(
        onTap: ()=> Navigator.of(context).push(new MaterialPageRoute(
            builder: (context)=>
            new ProductDetails(
              product_detail_name: prod_name,
              product_detail_picture:prod_picture ,
              product_detail_price:prod_price ,
            ))),
              child: GridTile(
                  footer:Container(
                    color: Colors.white70,
                    child: ListTile(
                      leading:Text(prod_name,
                          style: TextStyle(
                          fontWeight: FontWeight.bold),
                      ),
                      title: Text("\$$prod_price",style: TextStyle(color: Colors.red,fontWeight: FontWeight.bold),),
                    ),
                  ),child: Image.asset(prod_picture,
              fit: BoxFit.cover,)),
      ),)) ,
    );
   }
}



