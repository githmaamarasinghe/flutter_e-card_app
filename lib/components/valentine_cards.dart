import 'package:flutter/material.dart';
import 'package:flutter_app/pages/cart.dart';

class ValentineCards extends StatefulWidget {
  @override
  _ValentineCardsState createState() => _ValentineCardsState();
}

class _ValentineCardsState extends State<ValentineCards> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        elevation: 0.1,
        backgroundColor: Colors.blueGrey ,
        title:Text("E-Cards"),
        actions: [
          new IconButton(icon: Icon(Icons.search),color: Colors.white, onPressed: (){}),
          new IconButton(icon: Icon(Icons.shopping_cart),color: Colors.white, onPressed: (){
            Navigator.push(context, MaterialPageRoute(builder: (context)=> new Cart()));
          })],
      ),
      body: new ListView(
        children :[
        Row(
          children: [

            Expanded(child:

            Image(image: AssetImage('images/card1.jpg'),width: 120.0,height: 120.0,alignment: Alignment.topLeft,),
            ),
            Text('Price :\$12',style: TextStyle(color: Colors.deepOrange,fontWeight: FontWeight.bold),),
            new IconButton(icon: Icon(Icons.shopping_cart,color: Colors.deepOrange), onPressed: (){

            }),
            new IconButton(icon: Icon(Icons.favorite_border,color: Colors.deepOrange), onPressed: (){}),
          ],

        ),
          Divider(),
          Row(
            children: [

              Expanded(child:

              Image(image: AssetImage('images/handmadecard3.jpg'),width: 90.0,height: 90.0,alignment: Alignment.topLeft,),
              ),
              Text('Price :\$12',style: TextStyle(color: Colors.deepOrange,fontWeight: FontWeight.bold),),
              new IconButton(icon: Icon(Icons.shopping_cart,color: Colors.deepOrange), onPressed: (){

              }),
              new IconButton(icon: Icon(Icons.favorite_border,color: Colors.deepOrange), onPressed: (){}),
            ],

          ),
        ],

      ),


    );
  }
}
