import 'package:flutter/material.dart';
import 'package:flutter_app/components/valentine_cards.dart';

class HorizontalList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80.0,

      child: ListView(
        scrollDirection: Axis.horizontal,
      children: [
        Category(
          Image_location:'images/love.jpg' ,
          Image_captions: 'Valentine',
  ),
        Category(
          Image_location:'images/birthday.jpg' ,
          Image_captions: 'Birthday',
        ),
        Category(
          Image_location:'images/valentine.jpg' ,
          Image_captions: 'Greetings',
        ),
        Category(
          Image_location:'images/valentine.jpg' ,
          Image_captions: 'Wedding',
        ),




      ],
      ),
    );

  }
}

class Category extends StatelessWidget {
  final String Image_location;
  final String Image_captions;

   Category({
     this.Image_location,
     this.Image_captions
   });

  @override
  Widget build(BuildContext context) {
    return Padding(padding: const EdgeInsets.all(2.0),
    child: InkWell(onTap: (){
      Navigator.push(context, MaterialPageRoute(builder: (context)=> new ValentineCards()));
    },
    child: Container(
      width: 95.0,
      child: ListTile(
      title: Image.asset(Image_location,
      width: 100.0,
      height: 50.0,),
      subtitle:Container (
        alignment:Alignment.topCenter ,
        child: Text(Image_captions),
      ),
      ),
    ),
    ),
    );
  }
}

