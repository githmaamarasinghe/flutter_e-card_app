import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';

import 'package:flutter_app/components/horizontal_listview.dart';
import 'package:flutter_app/components/products.dart';
import 'package:flutter_app/pages/cart.dart';

void main() {
  runApp(
     MaterialApp(
       debugShowCheckedModeBanner: false,
       home: HomePage(),
     )

  );
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
  Widget image_carousel = new Container(
    height: 170.0,
    child: new Carousel(
      boxFit:BoxFit.cover ,
      indicatorBgPadding: 5,
      images: [
        AssetImage('images/card2.png'),
        AssetImage('images/card3.jpg'),
        AssetImage('images/card5.png'),

      ],
      autoplay: true,
      dotSize: 3,
      animationCurve: Curves.fastOutSlowIn,
      animationDuration: Duration(milliseconds: 1000),
    ),
  );
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.blueGrey,
        title:Text("E-Cards"),
        actions: [
          new IconButton(icon: Icon(Icons.search),color: Colors.white, onPressed: (){}),
          new IconButton(icon: Icon(Icons.shopping_cart),color: Colors.white, onPressed: (){
            Navigator.push(context, MaterialPageRoute(builder: (context)=> new Cart()));
          })],
      ),
      drawer: new Drawer(
        child: new ListView(
          children: [
            new UserAccountsDrawerHeader(accountName: Text('Githma Amarasinghe'), accountEmail: Text('githmaamrasinghe@gmail.com'),
            currentAccountPicture: GestureDetector(
              child: new CircleAvatar(
                backgroundColor: Colors.grey,
                child: Icon(Icons.person,color: Colors.white,),
              ),
            ),
              decoration: new BoxDecoration(
                color: Colors.blueGrey
              ),
            ),
              InkWell(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context)=> new HomePage()));
                },
                  child:ListTile(
                    title: Text('Home page'),
                    leading:Icon(Icons.home),
                  ),
              ),
            InkWell(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context)=> new HomePage()));
              },
              child:ListTile(
                title: Text('My Account'),
                leading:Icon(Icons.person),
              ),
            ),
            InkWell(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context)=> new Cart()));},
              child:ListTile(
                title: Text('My Orders'),
                leading:Icon(Icons.shopping_basket),
              ),
            ),
            InkWell(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context)=> new HomePage()));
              },
              child:ListTile(
                title: Text('Categories'),
                leading:Icon(Icons.dashboard),
              ),
            ),


          ],
        ),
      ),
      body: new ListView(
        children: [
          image_carousel,
          new Padding(padding:const EdgeInsets.all(8.0),//padding widget
           child: new Text('Categories',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16.0),),
          ),
        HorizontalList(),
          new Padding(padding:const EdgeInsets.all(20.0),//padding widget
            child: new Text('Recent Products',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16.0),),
          ),
          Container(
            height: 250.0,
            child: Products(),
          ),


        ],
      ),
    );

  }
}



