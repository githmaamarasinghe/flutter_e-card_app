import 'package:flutter/material.dart';
class Cart extends StatefulWidget {
  @override
  _CartState createState() => _CartState();
}

class _CartState extends State<Cart> {
  var Product_on_the_cart = [
  {
  "name":"Christmas Card",
  "picture":"images/handmadecard3.jpg",
  "price":20,
},

];

@override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.blueGrey,
        title:Text("Cart"),
        actions: [
          new IconButton(icon: Icon(Icons.search),color: Colors.white, onPressed: (){}),
        ],
      ),
      bottomNavigationBar: new Container(
        color: Colors.white,
        child: Row(
          children: [
            Expanded(child: ListTile(
              title: new Text("Total :"),
              subtitle: new Text("\$0"),
            )),
            Expanded(
              child: new MaterialButton(onPressed: (){},
              child: new Text("Check Out",style: TextStyle(
                color: Colors.white
              ),
              ),
                color: Colors.deepOrange,
              ),
            )
          ],
        ),
      ),
    );
  }
}
