import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/pages/cart.dart';

class ProductDetails extends StatefulWidget {

  final product_detail_name;
  final product_detail_price;
  final product_detail_picture;
  final product_description;

  const ProductDetails({
   this.product_detail_name,
    this.product_detail_price,
    this.product_detail_picture,
    this.product_description});


  @override
  _ProductDetailsState createState() => _ProductDetailsState();
}

class _ProductDetailsState extends State<ProductDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        elevation: 0.1,
        backgroundColor: Colors.blueGrey ,
        title:Text("E-Cards"),
        actions: [
          new IconButton(icon: Icon(Icons.search),color: Colors.white, onPressed: (){}),
          new IconButton(icon: Icon(Icons.shopping_cart),color: Colors.white, onPressed: (){
            Navigator.push(context, MaterialPageRoute(builder: (context)=> new Cart()));
          })],
      ),

      body: new ListView(
        children: [
          new Container(
            height: 300.0,
            child: GridTile(
                child: Container(
                  color: Colors.white,
                  child: Image.asset(widget.product_detail_picture),

                ),
              footer: new Container(
                color: Colors.white70,
                child: ListTile(
                  title: new Text(widget.product_detail_name,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16.0,
                  ),),
                  subtitle: Text("\$${widget.product_detail_price}",
                  style: TextStyle(color: Colors.red,fontSize: 16.0,
                  fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ),
          ),
          Row(
            children: [
              Expanded(child: MaterialButton(onPressed: (){},color: Colors.white,elevation: 0.2,
              child: Row(
                children: [
                  Expanded(child: new Text("Amount"),
                  ),
                  Expanded(child: new Icon(Icons.arrow_drop_down)),
                ],
              ),
              ),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(child: MaterialButton(onPressed: (){},color: Colors.deepOrange,
              textColor: Colors.white,elevation: 0.2,
              child: Text("Buy now"),
              ),

              ),
              new IconButton(icon: Icon(Icons.shopping_cart,color: Colors.deepOrange), onPressed: (){
                Navigator.push(context, MaterialPageRoute(builder: (context)=> new Cart()));
              }),
              new IconButton(icon: Icon(Icons.favorite_border,color: Colors.deepOrange), onPressed: (){}),
            ],

          ),

           Divider(),
          ListTile(
              title: new Text('Product Description'),
              subtitle: Text('cards come in two sizes 6 inch square or 210mm x 148mm.They are complimented with a coloured envelope and make the perfect keepsake for family and friends.'
                  'They are sure to delight the recipient. We can send your cards direct to the recipient or to you.All cards are dispatched in double board envelopes to ensure they arrive in pristine condition.'),
            ),
          Divider(),
          new Row(
            children: [
              Padding(padding: const EdgeInsets.fromLTRB(12, 5, 5, 5),
              child: Text("Product Name"),)
            ],
          )
          

        ],
      ),
    );
  }
}
